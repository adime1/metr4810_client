

$(document).ready(function(){


	var $myCanvas = $('#html-canvas');
	var pos_1_player = document.getElementById("pos_1");
	var pos_2_player = document.getElementById("pos_2");
	var tc_left = 0;
	var tc_right = 0;
	var key_capture = false
	var ip_address = "0.0.0.0";

	var http_rw = 0;
	var http_ts_speed = 0;
	var http_ts_dir = 0;
	var http_ts_stop = 0;
	var http_ch1_speed = 0;
	var http_ch1_dir = 0;
	var http_ch2_speed = 0;
	var http_ch2_dir = 0;
	var http_ch3_speed = 0;
	var http_ch3_dir = 0;
	var http_ch4_speed = 0;
	var http_ch4_dir = 0;

	$.fn.sendData = function(rw, ts_speed, ts_dir, ts_stop, ch1_speed, ch1_dir, ch2_speed, ch2_dir, ch3_speed, ch3_dir, ch4_speed, ch4_dir)
	{
		data = "rw=" + rw + ", " + "tsSpeed=" + ts_speed + ", " + "tsDir=" + ts_dir + ", " + "tsStop=" + ts_stop + ", "
		+ "ch1Speed=" + ch1_speed + ", " + "ch1Dir=" + ch1_dir + ", " + "ch2Speed=" + ch2_speed + ", " + "ch2Dir=" + ch2_dir + ", "
		+ "ch3Speed=" + ch3_speed + ", " + "ch3Dir=" + ch3_dir + ", " + "ch4Speed=" + ch4_speed + ", " + "ch4Dir=" + ch4_dir;
		fetch(("http://" + ip_address + ":80"), {
													method: 'POST',
													body: (data),
													credentials: 'same-origin'
									}).catch(function(err) {
													alert(err);
									});
	}




	$(document).keypress(function(e) {
		if(key_capture)
		{
			if(e.which == 44) { //TS_LEFT <
		    $.fn.sendData(0,100,0,0,0,0,0,0,0,0,0,0);
		  }

			if(e.which == 46) { //TS_RIGHT >
		     $.fn.sendData(0,100,1,0,0,0,0,0,0,0,0,0);
		  }

			if(e.which == 119) { // UP W
		     $.fn.sendData(0,0,0,0,100,0,0,0,0,0,0,0);
		  }

			if(e.which == 115) { // DOWN S
		     $.fn.sendData(0,0,0,0,100,1,0,0,0,0,0,0);
		  }
			if(e.which == 97) { // STRECTCH A
		     $.fn.sendData(0,0,0,0,0,0,100,0,0,0,0,0);
		  }
			if(e.which == 100) { // SQUEEZE D
		    $.fn.sendData(0,0,0,0,0,0,100,1,0,0,0,0);
		  }
			if(e.which == 122) { // OUT Z
		     $.fn.sendData(0,0,0,0,0,0,0,0,100,0,0,0);
		  }
			if(e.which == 120) { // IN X
		     $.fn.sendData(0,0,0,0,0,0,0,0,100,1,0,0);
		  }
			if(e.which == 32) { // SpaceBar full stop
		     $.fn.sendData(0,0,0,0,0,0,0,0,0,0,0,0);
		  }
			if(e.which == 116) { // CHANGE TRACK T
				$myCanvas.clearCanvas();
				if(tc_left == 0)
				{
					tc_left = 1;
					pos_1.pause()
					pos_2.load();
					pos_2.play();
					$myCanvas.drawLine({
						strokeStyle: 'grey',
						strokeWidth: 7,
						x1: 102, y1: 97, //use y1 = 60 or 100
						x2: 117, y2: 77,
					});

				}
				else
				{
					tc_left = 0;
					pos_2.pause()
					pos_1.load();
					pos_1.play();
					$myCanvas.drawLine({
						strokeStyle: 'grey',
						strokeWidth: 7,
						x1: 102, y1: 55, //use y1 = 60 or 100
						x2: 117, y2: 75,
					});
				}
		  }
		}

	});

	$("#keycapture_button").click(function(){
		if(key_capture)
		{
			key_capture = false;
			$("#keycapture_button").css("background-color", "red");
			$("#keycapture_button").text("Disabled");
		}
		else
		{
			key_capture = true;
			$("#keycapture_button").css("background-color", "green");
			$("#keycapture_button").text("Enabled");
		}
});

$("#ip_address_button").click(function(){
		ip_address = $("#ip_addr").val();
		$("#ip_addr").val("");
});


	$myCanvas.drawArc({
	  fillStyle: 'red',
	  strokeStyle: 'red',
	  strokeWidth: 0,
	  x: 10, y: 10,
	  radius: 5,
	  // start and end angles in degrees
	  start: 0, end: 360
	});

	$myCanvas.drawLine({
		strokeStyle: 'grey',
		strokeWidth: 7,
		x1: 102, y1: 55, //use y1 = 60 or 100
		x2: 117, y2: 75,
	});

	$("#tc").click(function(){
		$myCanvas.clearCanvas();
  	if(tc_left == 0)
		{
			tc_left = 1;
			pos_1.pause()
			pos_2.load();
			pos_2.play();
			$myCanvas.drawLine({
				strokeStyle: 'grey',
				strokeWidth: 7,
				x1: 102, y1: 97, //use y1 = 60 or 100
				x2: 117, y2: 77,
			});

		}
		else
		{
			tc_left = 0;
			pos_2.pause()
			pos_1.load();
			pos_1.play();
			$myCanvas.drawLine({
				strokeStyle: 'grey',
				strokeWidth: 7,
				x1: 102, y1: 55, //use y1 = 60 or 100
				x2: 117, y2: 75,
			});
		}
});




});
